//
//  DetailsViewController.swift
//  HomeworkCollectionView
//
//  Created by user153878 on 5/1/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userCity: UILabel!
    
    private var contact: Contact? = nil
    var contactId = -1 {
        didSet {
            contact = Contacts.contacts.all.filter{ $0.id == contactId }[0]
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fillData()
        // Do any additional setup after loading the view.
    }
    
    func fillData(){
        if contact != nil {
            userName.text = "\(contact!.fullName)"
            userEmail.text = "\(contact!.userMail)"
            userCity.text = "\(contact!.city)"
            userImage.image = contact!.userImage
        }
    }

}
