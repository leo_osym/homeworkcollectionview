//
//  CollectionHeader.swift
//  HomeworkCollectionView
//
//  Created by user153878 on 5/1/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import UIKit

class CollectionHeader: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
}
