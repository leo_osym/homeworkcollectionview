//
//  Contacts.swift
//  HomeworkContactList
//
//  Created by user153878 on 4/25/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import Foundation
import UIKit

class Contacts
{
    public static var contacts = Contacts()
    public static var images = ["icon_1","icon_2","icon_3","icon_4","icon_5"]
    
    public var all = Array<Contact>()
    
    private var categories = Array<String>()
    
    public func getCities()->[String]{
        var arr = Array<String>()
        for item in all {
            arr.append(item.city)
        }
        return arr
    }
    
    public func getUniqueCities()->[String]{
        categories = getCities()
        return Array(Set(categories)).sorted()
    }
    
    // populate with initial data
    init(){
        all.append(Contact(
            userId: 0,
            userName: "Maxine Caulfield",
            userEmail: "max_caulfield@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Arcadia Bay, OR"))
        
        all.append(Contact(
            userId: 1,
            userName: "Dean Moriarty",
            userEmail: "dean_moriarty@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "New York, NY"))
        
        all.append(Contact(
            userId: 2,
            userName: "Leo Auffmann",
            userEmail: "leo_auffmann@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Greentown, IL"))
        
        all.append(Contact(
            userId: 3,
            userName: "Nathan Prescott",
            userEmail: "nate_prescott@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Arcadia Bay, OR"))
        
        all.append(Contact(
            userId: 4,
            userName: "Chloe Price",
            userEmail: "chloe_price@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Arcadia Bay, OR"))
        
        all.append(Contact(
            userId: 5,
            userName: "Tom Spaulding",
            userEmail: "tom_spolding@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Greentown, IL"))
        
        all.append(Contact(
            userId: 6,
            userName: "Sal Paradise",
            userEmail: "sal_paradise@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "New York, NY"))
        
        all.append(Contact(
            userId: 7,
            userName: "Doug Spaulding",
            userEmail: "doug_spolding@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Greentown, IL"))
        
        all.append(Contact(
            userId: 8,
            userName: "Kate Marsh",
            userEmail: "super_kate@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Arcadia Bay, OR"))
        
        all.append(Contact(
            userId: 9,
            userName: "Mark Jeffenson",
            userEmail: "mark_jefferson@gmail.com",
            userImage: UIImage(named: Contacts.images.randomElement() ?? "default")!,
            userCity: "Arcadia Bay, OR"))
    }
}
