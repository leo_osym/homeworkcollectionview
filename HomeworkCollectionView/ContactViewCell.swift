//
//  ContactViewCell.swift
//  HomeworkCollectionView
//
//  Created by user153878 on 5/1/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import UIKit

class ContactViewCell: UICollectionViewCell {
    public var userId = -1
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userCity: UILabel!
    
}
