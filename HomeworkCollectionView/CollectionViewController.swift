//
//  CollectionViewController.swift
//  HomeworkCollectionView
//
//  Created by user153878 on 5/1/19.
//  Copyright © 2019 Leonid Osym. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CollectionViewController: UICollectionViewController {
    
    private var contacts = Contacts.contacts.all
    private var categories = Contacts.contacts.getCities()
    private var uniqueCategories = Contacts.contacts.getUniqueCities()
    var chosenCellId = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        //collectionView?.contentInsetAdjustmentBehavior = .always
        collectionView.delegate = self
        collectionView.dataSource = self
        // add title
        self.navigationItem.title = "Contact List"
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: UICollectionViewDataSource
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return uniqueCategories.count
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        for i in (0..<uniqueCategories.count) {
            if section == i {
                // count elements in section
                return categories.filter{$0 == uniqueCategories[i]}.count
            }
        }
        return 1
    }

    // MARK: - Fill cells
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCustomCell", for: indexPath) as? ContactViewCell
            else { fatalError("Fatal error!")}

        var currentCityContacts = contacts.filter{$0.city == uniqueCategories[indexPath.section]}
        
        cell.userId = currentCityContacts[indexPath.row].id
        cell.userName.text = currentCityContacts[indexPath.row].fullName
        cell.userCity.text = currentCityContacts[indexPath.row].city
        cell.userImage.image = currentCityContacts[indexPath.row].userImage
        
        return cell
    }
    
    // MARK: - Add text to header
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? CollectionHeader{
            sectionHeader.headerLabel.text = "City: \(uniqueCategories[indexPath.section])"
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    // MARK: - On cell choose
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let currentCell = collectionView.cellForItem(at: indexPath)! as? ContactViewCell {
            chosenCellId = currentCell.userId
            performSegue(withIdentifier: "ShowDetails", sender: currentCell)
        }
    }
    
     // MARK: - Go to Details controller
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetails" {
            if let dvc = segue.destination as? DetailsViewController {
                dvc.contactId = chosenCellId
            }
        }
     }
    
}

extension CollectionViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 150)
    }
}
